const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers")

router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/createTask", (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Delete

router.delete("/deleteTask/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//updating
router.put("/updateTask/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//ACTIVITY STARTS HERE

//specific task
router.get("/:id", (req, res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//complete task
router.put("/:id/complete", (req, res) => {
	taskControllers.completeTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// router
module.exports = router;