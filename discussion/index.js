const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");

const app = express();
const port = 4000;

// middlewares

mongoose.connect("mongodb+srv://mgcuaresma:admin@wdc028-course-booking.cgrme.mongodb.net/session33?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology :true
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("Succeessfully connected to MongoDB"));

app.use('/tasks', taskRoutes);

app.listen(port, () => {
	console.log(`Server is running at port ${port}`)
});